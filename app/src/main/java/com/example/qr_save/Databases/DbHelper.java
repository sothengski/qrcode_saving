package com.example.qr_save.Databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.qr_save.Model.Code;
import com.example.qr_save.Model.ListItem;

import java.util.ArrayList;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class DbHelper extends SQLiteOpenHelper
{
//    public static final String TABLE_NAME = "mytable";
    public static final String DATABASE_NAME = "qrdb.db";
    private static final int DATABASE_VERSION = 1;

//    public static final String COL_1 = "id";
//    public static final String COL_2 = "code";
//    public static final String COL_3 = "type";

    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
//
//    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
//    }
    static {
        //register our models
        cupboard().register(Code.class);

}

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
//        db.execSQL("create table "+ TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
//                COL_2 + "TEXT," + COL_3 + "TEXT)");
//        String createTable = "CREATE TABLE " + TABLE_NAME + " (id INTEGER PRIMARY KEY AUTOINCREMENT," +
//                "code TEXT, type TEXT)";
//        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
//        onCreate(db);
    }

//    public boolean insertData(String code, String type)
//    {
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(COL_2, code);
//        contentValues.put(COL_3,type);
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        long result = db.insert(TABLE_NAME,null,contentValues);
//
//        if (result == -1)
//            return false;
//        else
//            return true;
//    }
//
//    public ArrayList<ListItem> getAllInformation()
//    {
//        ArrayList<ListItem> arrayList = new ArrayList<>();
//
//        SQLiteDatabase database = this.getReadableDatabase();
//        Cursor cursor = database.rawQuery("select * from "+TABLE_NAME,null);
//
//        if (cursor != null)
//        {
//            while (cursor.moveToNext())
//            {
//                int id = cursor.getInt(0);
//                String code = cursor.getString(1);
//                String type = cursor.getString(2);
//
//                ListItem listItem = new ListItem(id,code,type);
//
//                arrayList.add(listItem);
//            }
//        }
//
//        cursor.close();
//        return arrayList;
//    }
//
//    public void deleteRow(int value)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DELETE FROM "+TABLE_NAME + "= WHERE "+COL_1+"='"+value+"'");
//    }
}

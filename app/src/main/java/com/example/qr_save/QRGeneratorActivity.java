package com.example.qr_save;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.qr_save.Adapters.MyAdapter;
import com.example.qr_save.Databases.DbHelper;
import com.example.qr_save.Model.Code;
import com.example.qr_save.Model.ListItem;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.BarcodeFormat;

import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class QRGeneratorActivity extends AppCompatActivity {

    RecyclerView.Adapter adapter;
    ImageView imageView;
    Button buttonGenerator;
    //    Button btnScan;
    EditText editText;
    String EditTextValue;
    Thread thread;
    public final static int QRcodeWidth = 350;
    Bitmap bitmap;
    private ProgressBar loader;
    TextView tv_qr_readTxt;
    private Bitmap bitmapResult;
    Button buttonSave;
    Button buttonReset;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_generator);

        imageView = findViewById(R.id.imgResult);
        editText = findViewById(R.id.txtQR);
        buttonGenerator = findViewById(R.id.btnGenerate);
//        btnScan = findViewById(R.id.btnScan);
        tv_qr_readTxt = findViewById(R.id.txtSaveHint);
        loader = findViewById(R.id.loader);
        buttonSave = findViewById(R.id.btnSave);
        buttonReset = findViewById(R.id.btnReset);
//        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

        buttonGenerator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!editText.getText().toString().isEmpty()) {
                    EditTextValue = editText.getText().toString();

                    try {
                        System.out.println("text::"+EditTextValue);
                        bitmap = TextToImageEncode(EditTextValue);
                        System.out.println("bitmap::"+bitmap);
                        //bitmap::android.graphics.Bitmap@5569aa3 == Hello

                        imageView.setImageBitmap(bitmap);
                        showLoadingVisible(false);
                        buttonSave.setVisibility(View.VISIBLE);


                    } catch (WriterException | InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    editText.requestFocus();
                    Toast.makeText(QRGeneratorActivity.this, "Please Enter Your Scanned Test", Toast.LENGTH_LONG).show();
                }

            }
        });
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                imageView.setImageBitmap(null);
                buttonSave.setVisibility(View.INVISIBLE);
            }
        });

        buttonSave.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                boolean save;
//                String result;
                initializeData(editText.getText().toString(),"QR_CODE");
//                try {
//                    String savePath = Environment.getExternalStorageDirectory() + "/QRCode/";
//                    save = QrC.save(savePath, "WebSite QR code", bitmapResult, QRGContents.ImageType.IMAGE_JPEG);
//                    result = save ? "Image Saved" : "Image Not Saved";
//                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        });
    }

    private void initializeData(String name, String type) {
        Code codeObj = new Code(name, type);
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id = cupboard().withDatabase(db).put(codeObj);
//        listItems.clear();
//        adapter.notifyDataSetChanged();
        Cursor codes = cupboard().withDatabase(db).query(Code.class).orderBy("_id DESC").getCursor();
        try {
            // Iterate Bunnys
            QueryResultIterable<Code> itr = cupboard().withCursor(codes).iterate(Code.class);
            for (Code bunny : itr) {
                // do something with bunny
                ListItem listItem = new ListItem(bunny._id, bunny.name, bunny.type);
//                    listItems.add(listItem);
//                    adapter = new MyAdapter(this,listItems, this);
//                    recyclerView.setAdapter(adapter);
            }
        } finally {
            // close the cursor
            codes.close();
        }
    }

    Bitmap TextToImageEncode(String Value) throws WriterException, InterruptedException {
        BitMatrix bitMatrix;
        showLoadingVisible(true);
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor) : getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
//        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmapResult = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
        bitmapResult.setPixels(pixels, 0, 350, 0, 0, bitMatrixWidth, bitMatrixHeight);

        return bitmapResult;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.e("Scan*******", "Cancelled scan");

            } else {
                Log.e("Scan", "Scanned");

                tv_qr_readTxt.setText(result.getContents());
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void showLoadingVisible(boolean visible){
        if(visible){
            imageView.setImageBitmap(null);
        }

        loader.setVisibility(
                (visible) ? View.VISIBLE : View.GONE
        );
    }


}
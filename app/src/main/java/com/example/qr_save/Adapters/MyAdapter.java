package com.example.qr_save.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.qr_save.Model.ListItem;
import com.example.qr_save.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyAdapterViewHolder> {

    private Activity activity;
    List<ListItem> listItems;
    Context context;
    private Bitmap bitmapResult;
    public final static int QRcodeWidth = 350;
    Bitmap bitmap;
    String qrResulttxt;
    ImageView qrcodeImage;

    public MyAdapter(Activity activity, List<ListItem> listItems, Context context) {
        this.activity = activity;
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public MyAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater inflater = activity.getLayoutInflater();
//        View view = inflater.inflate(R.layout.list_item_layout,parent,false);
//        MyAdapterViewHolder myAdapterViewHolder= new MyAdapterViewHolder(view);
//        return myAdapterViewHolder;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_layout, parent, false);
        return new MyAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyAdapterViewHolder myAdapterViewHolder, int position) {
        ListItem listItem = listItems.get(position);
        myAdapterViewHolder.textView_Id.setText(String.valueOf(listItem.get_Id()));
        myAdapterViewHolder.textViewType.setText(listItem.getType());
        myAdapterViewHolder.textViewCode.setText(listItem.getCode());
        myAdapterViewHolder.container.setOnClickListener(onClickListener(position));
//            myAdapterViewHolder.ivQRCode.setImageBitmap(listItem.getBitmap1());
        Linkify.addLinks(myAdapterViewHolder.textViewCode, Linkify.ALL);
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(activity);
//                final Dialog dialog = new Dialog(mainActivity);
                dialog.setContentView(R.layout.qrcode_detail_dialog_layout);
                dialog.setTitle("Detail");
                dialog.setCancelable(true); // dismiss when touching outside Dialog

                // set the custom dialog components - texts and image
                TextView qrResult = dialog.findViewById(R.id.txtdialogQrresult);
                TextView qrType = dialog.findViewById(R.id.txtdialogtype);
                qrcodeImage = dialog.findViewById(R.id.ivdialogqrcode);


                Button backbutton = dialog.findViewById(R.id.btnback);
                backbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
//                        alertDialog.dismiss();
                    }
                });
                setDataToView(qrResult, qrType, position);
                dialog.show();

                if (!qrResulttxt.isEmpty()) {
                    try {
                        bitmap = TextToImageEncode(qrResulttxt);
                        qrcodeImage.setImageBitmap(bitmap);
//                        buttonSave.setVisibility(View.VISIBLE);

                    } catch (WriterException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "Result not Found", Toast.LENGTH_LONG).show();
                }
            }

            Bitmap TextToImageEncode(String Value) throws WriterException {
                BitMatrix bitMatrix;
                try {
                    bitMatrix = new MultiFormatWriter().encode(
                            Value,
                            BarcodeFormat.DATA_MATRIX.QR_CODE,
                            QRcodeWidth, QRcodeWidth, null
                    );

                } catch (IllegalArgumentException Illegalargumentexception) {

                    return null;
                }
                int bitMatrixWidth = bitMatrix.getWidth();

                int bitMatrixHeight = bitMatrix.getHeight();

                int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

                for (int y = 0; y < bitMatrixHeight; y++) {
                    int offset = y * bitMatrixWidth;

                    for (int x = 0; x < bitMatrixWidth; x++) {

                        pixels[offset + x] = bitMatrix.get(x, y) ?
                                ContextCompat.getColor(context, R.color.QRCodeBlackColor) : ContextCompat.getColor(context, R.color.QRCodeWhiteColor);
//                                getResources().getColor(R.color.QRCodeBlackColor) : getResources().getColor(R.color.QRCodeWhiteColor);
                    }
                }
//        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
                bitmapResult = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
                bitmapResult.setPixels(pixels, 0, 350, 0, 0, bitMatrixWidth, bitMatrixHeight);
                return bitmapResult;
            }

            private void setDataToView(TextView qrResult, TextView qrType, int position) {
                qrResult.setText(listItems.get(position).getCode());
                qrType.setText(listItems.get(position).getType());
                qrResulttxt = qrResult.getText().toString();
            }
        };
    }


    @Override
    public int getItemCount() {
        return listItems.size();
//        return 0;
    }


    public class MyAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewCode, textViewType, textView_Id;
        private View container;

        public MyAdapterViewHolder(final View itemView) {
            super(itemView);
            textView_Id = itemView.findViewById(R.id.txtView_Id);
            textViewCode = itemView.findViewById(R.id.txtViewCode);
            textViewType = itemView.findViewById(R.id.txtViewType);
            container = itemView.findViewById(R.id.cardView);
        }
    }
}

package com.example.qr_save.Model;

import android.graphics.Bitmap;

public class ListItem {
    private long _id;
    private String code;
    private String type;
    private Bitmap bitmap1;

    public ListItem(long _id,String code, String type) {
        this._id = _id;
        this.code = code;
        this.type = type;
//        this.bitmap1 = bitmap1;
    }
    public long get_Id() {
        return  _id;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public Bitmap getBitmap1() {
        return bitmap1;
    }

    public void setBitmap1(Bitmap bitmap1) {
        this.bitmap1 = bitmap1;
    }
}
